package fr.efrei.backend.web;

import fr.efrei.backend.domain.Student;
import fr.efrei.backend.service.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class StudentRessource {

    public final StudentService studentService;

    public StudentRessource(StudentService studentService) {
        this.studentService = studentService;
    }
        @GetMapping("/students")
        public List<Student> getAllItems(){
            return studentService.findAll();
        }

    @GetMapping("/students/{id}")
    public Optional<Student> getSpecificStudent(@PathVariable Integer id){
        Optional<Student> student = studentService.findStudent(id);
            return  student;
    }

    @DeleteMapping("/students/{id}")
    public Optional<Student> deleteSpecificStudent(@PathVariable Integer id){
        return studentService.deleteStudent(id);
    }

    @PostMapping("/students")
    public Student createStudent(@RequestBody Student student){
        return studentService.createStudent(student);
    }

    @PutMapping("/students")
    public Optional<Student> updateStudent(@RequestBody Student student){
        return studentService.updateStudent(Optional.ofNullable(student));
    }

    }
