package fr.efrei.backend.service;

import fr.efrei.backend.domain.Student;
import fr.efrei.backend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;
    public List<Student> findAll(){return studentRepository.findAll();}
   /*  
  public Optional<Student> findStudent(Integer idStudent){
        Optional<Student> tempStudent = studentRepository.findById(idStudent);
        return tempStudent;
        //return studentRepository.findById(idStudent);
    }
    */
  public Optional<Student> findStudent(Integer idStudent){
      Optional<Student> tempStudent = studentRepository.findById(idStudent);
      return tempStudent;
      //return null;
      //return studentRepository.findById(idStudent);
  }
    public Optional<Student> deleteStudent(Integer idStudent){
        Optional<Student> studentOptional = studentRepository.findById(idStudent);
        if (studentOptional.isPresent()) {
            studentRepository.deleteById(idStudent);
            return studentOptional;
        }
        return Optional.empty();
        //studentRepository.deleteById(idStudent);//
    }

    public Student createStudent(Student student){

      Student studentToAdd = new Student();
        studentToAdd.setName(student.getName());
        studentToAdd.setAge(student.getAge());
        studentToAdd.setId((studentRepository.findAll().size()+1)*3);

        return studentRepository.save(studentToAdd);
    }

    public Optional<Student> updateStudent(Optional<Student> updatedStudent) {
        Optional<Student> oldStudent = this.findStudent(updatedStudent.map(Student::getId).orElse((int) -1L));

        if (oldStudent.isPresent() && updatedStudent.isPresent()) {
            Student updatedInfoStudent = oldStudent.get(); // Start with the old student's values
            // Update age if provided, otherwise retain the old value
            updatedStudent.map(Student::getAge).ifPresent(updatedInfoStudent::setAge);
            // Update name if provided, otherwise retain the old value
            updatedStudent.map(Student::getName).ifPresent(updatedInfoStudent::setName);
            studentRepository.save(updatedInfoStudent);
            return Optional.of(updatedInfoStudent);
        }
        return Optional.empty();
    }

}
