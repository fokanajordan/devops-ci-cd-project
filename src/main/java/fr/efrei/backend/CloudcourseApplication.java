package fr.efrei.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudcourseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudcourseApplication.class, args);
	}

}
