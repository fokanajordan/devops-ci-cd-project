package fr.efrei.backend.web.rest;

import fr.efrei.backend.domain.Student;
import fr.efrei.backend.repository.StudentRepository;
import fr.efrei.backend.service.StudentService;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class StudentRessourceIT {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentService studentService;

    @Test
    @Transactional
    void createItems() throws Exception {
        //int databaseSizeBeforeCreate = studentRepository.findAll().size();
        int databaseSizeBeforeCreate = studentService.findAll().size();
        assertThat(databaseSizeBeforeCreate).isEqualTo(0);
        Student newStudent = new Student();
        newStudent.setName("Dummy");
        newStudent.setAge(20);
        studentService.createStudent(newStudent);
        /*System.out.print("Created student: ");
        System.out.println(studentService.findAll());
         */
        int databaseSizeAfterCreate = studentService.findAll().size();
        assertThat(databaseSizeAfterCreate).isEqualTo(databaseSizeBeforeCreate+1);
        Student createdStudent = studentService.findAll().get(0);
        assertThat(createdStudent.getName()).isEqualTo("Dummy");
        assertThat(createdStudent.getAge()).isEqualTo(20);
    }


    @Test
    @Transactional
    void deleteItems() throws Exception {
        Student newStudent = new Student();
        newStudent.setName("Dummy");
        newStudent.setAge(20);
        studentService.createStudent(newStudent);
        int databaseSizeBeforeCreate = studentService.findAll().size();
        assertThat(databaseSizeBeforeCreate).isEqualTo(1);
        Student firstStudent = studentRepository.findAll().get(0);
        studentService.deleteStudent(firstStudent.getId());
        int databaseSizeAfterCreate = studentService.findAll().size();
        assertThat(databaseSizeAfterCreate).isEqualTo(databaseSizeBeforeCreate-1);
        assertTrue(studentService.findStudent(firstStudent.getId()).isEmpty());

    }

    @Test
    @Transactional
    void updateStudent() throws Exception {
        Student newStudent = new Student();
        newStudent.setName("Dummy");
        newStudent.setAge(20);
        studentService.createStudent(newStudent);
        Integer studentId = studentService.findAll().get(0).getId();
        Student updatedStudent = new Student();
        updatedStudent.setId(studentId);
        updatedStudent.setName("UpdatedDummy");
        updatedStudent.setAge(25);
        studentService.updateStudent(Optional.of(updatedStudent));
        Student retrievedStudent = studentService.findStudent(studentId).get();
        assertThat(retrievedStudent).isNotNull();
        assertThat(retrievedStudent.getName()).isEqualTo("UpdatedDummy");
        assertThat(retrievedStudent.getAge()).isEqualTo(25);
    }

    @Test
    @Transactional
    void createStudentWithMissingInformation() throws Exception {
        int databaseSizeBeforeCreate = studentService.findAll().size();
        assertThat(databaseSizeBeforeCreate).isEqualTo(0);
        Student studentWithMissingInfo = new Student();
        studentWithMissingInfo.setName("IncompleteStudent");
        studentService.createStudent(studentWithMissingInfo);
        int databaseSizeAfterCreate = studentService.findAll().size();
        assertThat(databaseSizeAfterCreate).isEqualTo(databaseSizeBeforeCreate + 1);
        Student createdStudent = studentService.findAll().get(0);
        assertThat(createdStudent.getName()).isEqualTo("IncompleteStudent");
        assertThat(createdStudent.getAge()).isEqualTo(null);
    }

    @Test
    @Transactional
    void updateStudentWithPartialInformation() throws Exception {
        Student newStudent = new Student();
        newStudent.setName("Dummy");
        newStudent.setAge(20);
        studentService.createStudent(newStudent);
        Integer studentId = studentService.findAll().get(0).getId();
        Student updatedStudent = new Student();
        updatedStudent.setId(studentId);
        updatedStudent.setName("UpdatedDummy");
        studentService.updateStudent(Optional.of(updatedStudent));
        Student retrievedStudent = studentService.findStudent(studentId).get();
        assertThat(retrievedStudent).isNotNull();
        assertThat(retrievedStudent.getName()).isEqualTo("UpdatedDummy");
        assertThat(retrievedStudent.getAge()).isEqualTo(20);
    }

    @Test
    @Transactional
    void deleteNonExistingStudent() throws Exception {
        int databaseSizeBeforeDelete = studentService.findAll().size();
        assertThat(databaseSizeBeforeDelete).isEqualTo(0);
        studentService.deleteStudent(999);
        int databaseSizeAfterDelete = studentService.findAll().size();
        assertThat(databaseSizeAfterDelete).isEqualTo(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    void updateNonExistingStudent() throws Exception {
        int databaseSizeBeforeUpdate = studentService.findAll().size();
        assertThat(databaseSizeBeforeUpdate).isEqualTo(0);
        Student updatedStudent = new Student();
        updatedStudent.setId(999);
        updatedStudent.setName("UpdatedDummy");
        studentService.updateStudent(Optional.of(updatedStudent));
        int databaseSizeAfterUpdate = studentService.findAll().size();
        assertThat(databaseSizeAfterUpdate).isEqualTo(databaseSizeBeforeUpdate);
    }

}
